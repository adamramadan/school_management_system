<?php

namespace App\Http\Controllers\Classroom;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClassroom;
use App\Models\Classroom;
use App\Models\Grade;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classrooms = Classroom::all();
        $grades = Grade::all();
        return view('classrooms.classrooms', compact('classrooms', 'grades'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassroom $request)
    {
        $list_classes = $request->List_Classes;

        try {
            $validated = $request->validated();


            foreach ($list_classes as $list_class) {

                $my_classes = new Classroom();

                $my_classes->name_class = ['en' => $list_class['name_en'], 'ar' => $list_class['name_ar']];

                $my_classes->grade_id = $list_class['grade_id'];

                $my_classes->save();

            }

            toastr()->success(trans('messages.success'));
            return redirect()->route('classrooms.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            $classrooms = Classroom::findOrFail($request->id);

            $classrooms->update([

                $classrooms->name_class = ['ar' => $request->name_ar, 'en' => $request->name_en],
                $classrooms->grade_id = $request->grade_id,
            ]);
            toastr()->success(trans('messages.update'));
            return redirect()->route('classrooms.index');
        }

        catch
        (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $classrooms = Classroom::findOrFail($request->id)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('classrooms.index');
        
    }

    public function delete_all(Request $request)
    {
        $delete_all_id = explode(",", $request->delete_all_id);

        Classroom::whereIn('id', $delete_all_id)->Delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('classrooms.index');
    }

    public function filter_classes(Request $request)
    {
        $grades = Grade::all();
        $search = Classroom::select('*')->where('grade_id','=',$request->grade_id)->get();
        return view('classrooms.classrooms',compact('grades'))->withDetails($search);

    }
}
