<?php

namespace App\Http\Livewire;

use App\Models\BloodType;
use App\Models\MyParent;
use App\Models\Nationality;
use App\Models\ParentAttachment;
use App\Models\Religion;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddParent extends Component
{
    use WithFileUploads;

    public $successMessage = '';

    public $catchError, $updateMode = false, $photos, $show_table = true, $parent_id;

    public $currentStep = 1,

        // Father_INPUTS
        $email, $password,
        $father_name, $father_name_en,
        $father_id_number, $father_id_passport,
        $father_phone, $father_job, $father_job_en,
        $father_nationality_id, $father_blood_type_id,
        $father_address, $father_religion_id,

        // Mother_INPUTS
        $mother_name, $mother_name_en,
        $mother_id_number, $mother_id_passport,
        $mother_phone, $mother_job, $mother_job_en,
        $mother_nationality_id, $mother_blood_type_id,
        $mother_address, $mother_religion_id;

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'email' => 'required|email',
            'father_id_number' => 'required|string|min:10|max:10|regex:/[0-9]{9}/',
            'father_id_passport' => 'min:10|max:10',
            'father_phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'mother_id_number' => 'required|string|min:10|max:10|regex:/[0-9]{9}/',
            'mother_id_passport' => 'min:10|max:10',
            'mother_phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
        ]);
    }
    public function render()
    {
        return view('livewire.add-parent', [
            'nationalities' => Nationality::all(),
            'type_bloods' => BloodType::all(),
            'religions' => Religion::all(),
            'my_parents' => MyParent::all(),
        ]);
    }
    public function showformadd()
    {
        $this->show_table = false;
    }

    //firstStepSubmit
    public function firstStepSubmit()
    {
        $this->validate([
            'email' => 'required|unique:my_parents,email,' . $this->id,
            'password' => 'required',
            'father_name' => 'required',
            'father_name_en' => 'required',
            'father_job' => 'required',
            'father_job_en' => 'required',
            'father_id_number' => 'required|unique:my_parents,father_id_number,' . $this->id,
            'father_id_passport' => 'required|unique:my_parents,father_id_passport,' . $this->id,
            'father_phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'father_nationality_id' => 'required',
            'father_blood_type_id' => 'required',
            'father_religion_id' => 'required',
            'father_address' => 'required',
        ]);
        $this->currentStep = 2;
    }

    //secondStepSubmit
    public function secondStepSubmit()
    {

        $this->validate([
            'mother_name' => 'required',
            'mother_name_en' => 'required',
            'mother_id_number' => 'required|unique:my_parents,mother_id_number,' . $this->id,
            'mother_id_passport' => 'required|unique:my_parents,mother_id_passport,' . $this->id,
            'mother_phone' => 'required',
            'mother_job' => 'required',
            'mother_job_en' => 'required',
            'mother_nationality_id' => 'required',
            'mother_blood_type_id' => 'required',
            'mother_religion_id' => 'required',
            'mother_address' => 'required',
        ]);
        $this->currentStep = 3;
    }


    public function submitForm()
    {


        // Mother_INPUTS


        try {
            $my_parent = new MyParent();
            // Father_INPUTS
            $my_parent->email = $this->email;
            $my_parent->password = Hash::make($this->password);
            $my_parent->father_name = ['en' => $this->father_name_en, 'ar' => $this->father_name];
            $my_parent->father_id_number = $this->father_id_number;
            $my_parent->father_id_passport = $this->father_id_passport;
            $my_parent->father_phone = $this->father_phone;
            $my_parent->father_job = ['en' => $this->father_job_en, 'ar' => $this->father_job];
            // $my_parent->Passport_ID_Father = $this->Passport_ID_Father;
            $my_parent->father_nationality_id = $this->father_nationality_id;
            $my_parent->father_blood_type_id = $this->father_blood_type_id;
            $my_parent->father_religion_id = $this->father_religion_id;
            $my_parent->father_address = $this->father_address;

            // Mother_INPUTS




            $my_parent->mother_name = ['en' => $this->mother_name_en, 'ar' => $this->mother_name];
            $my_parent->mother_id_number = $this->mother_id_number;
            $my_parent->mother_id_passport = $this->mother_id_passport;
            $my_parent->mother_phone = $this->mother_phone;
            $my_parent->mother_job = ['en' => $this->mother_job_en, 'ar' => $this->mother_job];
            // $my_parent->Passport_ID_Mother = $this->Passport_ID_Mother;
            $my_parent->mother_nationality_id = $this->mother_nationality_id;
            $my_parent->mother_blood_type_id = $this->mother_blood_type_id;
            $my_parent->mother_religion_id = $this->mother_religion_id;
            $my_parent->mother_address = $this->mother_address;
            $my_parent->save();

            if (!empty($this->photos)) {
                foreach ($this->photos as $photo) {
                    $photo->storeAs($this->father_id_number, $photo->getClientOriginalName(), $disk = 'parent_attachments');
                    ParentAttachment::create([
                        'file_name' => $photo->getClientOriginalName(),
                        'parent_id' => MyParent::latest()->first()->id,
                    ]);
                }
            }

            $this->successMessage = trans('messages.success');
            $this->clearForm();
            $this->currentStep = 1;
        } catch (\Exception $e) {
            $this->catchError = $e->getMessage();
        };
    }

    public function edit($id)
    {



        $this->show_table = false;
        $this->updateMode = true;
        $my_parent = MyParent::where('id', $id)->first();
        $this->parent_id = $id;
        $this->email = $my_parent->email;
        $this->password = $my_parent->password;
        $this->father_name = $my_parent->getTranslation('father_name', 'ar');
        $this->father_name_en = $my_parent->getTranslation('father_name', 'en');
        $this->father_job = $my_parent->getTranslation('father_job', 'ar');;
        $this->father_job_en = $my_parent->getTranslation('father_job', 'en');
        $this->father_id_passport = $my_parent->father_id_passport;
        $this->father_id_number = $my_parent->father_id_number;
        $this->father_phone = $my_parent->father_phone;
        $this->father_nationality_id = $my_parent->father_nationality_id;
        $this->father_blood_type_id = $my_parent->father_blood_type_id;
        $this->father_address = $my_parent->father_address;
        $this->father_religion_id = $my_parent->father_religion_id;


        $this->mother_name = $my_parent->getTranslation('mother_name', 'ar');
        $this->mother_name_en = $my_parent->getTranslation('mother_name', 'en');
        $this->mother_job = $my_parent->getTranslation('mother_job', 'ar');;
        $this->mother_job_en = $my_parent->getTranslation('mother_job', 'en');
        $this->mother_id_number = $my_parent->mother_id_number;
        $this->mother_id_passport = $my_parent->mother_id_passport;
        $this->mother_phone = $my_parent->mother_phone;
        $this->mother_nationality_id = $my_parent->mother_nationality_id;
        $this->mother_blood_type_id = $my_parent->mother_blood_type_id;
        $this->mother_address = $my_parent->mother_address;
        $this->mother_religion_id = $my_parent->mother_religion_id;
    }

    public function firstStepSubmitEdit()
    {
        $this->updateMode = true;
        $this->currentStep = 2;
    }

    //secondStepSubmit_edit
    public function secondStepSubmitEdit()
    {
        $this->updateMode = true;
        $this->currentStep = 3;
    }

    public function submitFormEdit()
    {


        if ($this->parent_id) {
            $parent = MyParent::find($this->parent_id);
            $parent->update([
                'email' => $this->email,
                'password' => $this->password,
                'father_name' => ['en' => $this->father_name_en, 'ar' => $this->father_name],
                'father_id_passport' => $this->father_id_passport,
                'father_id_number' => $this->father_id_number,
                'father_phone' => $this->father_phone,
                'father_job' => ['en' => $this->father_job_en, 'ar' => $this->father_job],
                'father_nationality_id' => $this->father_nationality_id,
                'father_blood_type_id' => $this->father_blood_type_id,
                'father_address' => $this->father_address,
                'father_religion_id' => $this->father_religion_id,

                //Mother
                'mother_name' => ['en' => $this->mother_name_en, 'ar' => $this->mother_name],
                'mother_id_number' => $this->mother_id_number,
                'mother_id_passport' => $this->mother_id_passport,
                'mother_phone' => $this->mother_phone,
                'mother_job' => ['en' => $this->mother_job, 'ar' => $this->mother_job],
                'mother_nationality_id' => $this->mother_nationality_id,
                'mother_blood_type_id' => $this->mother_blood_type_id,
                'mother_religion_id' => $this->mother_religion_id

            ]);
        }



        return redirect()->to('/add_parent');
    }

    public function delete($id)
    {
        MyParent::findOrFail($id)->delete();
        return redirect()->to('/add_parent');
        // $this->successMessage = trans('messages.delete');
    }



    public function clearForm()
    {

        $this->email = '';
        $this->password = '';
        $this->name_father = '';
        $this->father_job = '';
        $this->father_job_en = '';
        $this->name_father = '';
        $this->father_id_number = '';
        $this->father_id_passport = '';
        $this->father_phone = '';
        $this->father_nationality_id = '';
        $this->father_blood_type_id = '';
        $this->father_address = '';
        $this->father_religion_id = '';





        $this->mother_name = '';
        $this->mother_job = '';
        $this->mother_job_en = '';
        $this->mother_name_en = '';
        $this->mother_id_number = '';
        $this->mother_id_passport = '';
        $this->mother_phone = '';
        $this->mother_nationality_id = '';
        $this->mother_blood_type_id = '';
        $this->mother_address = '';
        $this->mother_religion_id = '';
    }
    //back
    public function back($step)
    {
        $this->currentStep = $step;
    }
}
