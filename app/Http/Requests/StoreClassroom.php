<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClassroom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'List_Classes.*.name_ar' => 'required|unique:classrooms,name_class->ar,'.$this->id,
            'List_Classes.*.name_en' => 'required|unique:classrooms,name_class->en,'.$this->id,
            // 'List_Classes.*.name_ar' => 'unique',
            // 'List_Classes.*.name_en' => 'unique',
        ];
    }

    public function messages()
    {
        return [
            'name_ar.required' => trans('validation.required'),
            'name_ar.unique' => trans('validation.unique'),
            'name_en.required' => trans('validation.required'),
            'name_en.unique' => trans('validation.unique'),
        ];
    }
}
