<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSection extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_section_ar' => 'required',
            'name_section_en' => 'required',
            'grade_id' => 'required',
            'classroom_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name_section_ar.required' => trans('sections_trans.required_ar'),
            'name_section_en.required' => trans('sections_trans.required_en'),
            'grade_id.required' => trans('sections_trans.grade_id_required'),
            'classroom_id.required' => trans('sections_trans.class_id_required'),
        ];
    }
}
