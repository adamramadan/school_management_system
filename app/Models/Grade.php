<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;


class Grade extends Model
{
    use HasTranslations;
    use HasFactory;

    protected $fillable = ['name', 'notes'];

    public $translatable = ['name'];

    public function sections(){
        return $this->hasMany('App\Models\Section', 'grade_id');
    }
    
}
