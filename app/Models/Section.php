<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Section extends Model
{
    use HasFactory;

    use HasTranslations;
    public $translatable = ['name_section'];
    protected $fillable=['name_section','grade_id','classroom_id'];

    public function my_classes()
    {
        return $this->belongsTo('App\Models\Classroom', 'classroom_id');
    }

    // علاقة الاقسام مع المعلمين
    public function teachers()
    {
        return $this->belongsToMany('App\Models\Teacher', 'teacher_section');
    }
}
