<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Teacher extends Model
{
    use HasFactory;

    use HasTranslations;
    public $translatable = ['name'];
    protected $guarded = [];

    // علاقة بين المعلمين والتخصصات لجلب إسم التخصص
    public function specializations()
    {
        return $this->belongsTo('App\Models\Specialization', 'specialization_id');
    }

    // علاقة بين المعلمين والأنواع لجلب جنس المعلم
    public function genders()
    {
        return $this->belongsTo('App\Models\Gender', 'gender_id');
    }

    public function sections()
    {
        return $this->belongsToMany('App\Models\Section', 'teacher_section');
    }
}
