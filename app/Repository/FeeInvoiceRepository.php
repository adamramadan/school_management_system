<?php


namespace App\Repository;

use App\Models\Fee;
use App\Models\FeeInvoice;
use App\Models\Grade;
use App\Models\Student;
use App\Models\StudentAccount;
use Illuminate\Support\Facades\DB;

class FeeInvoiceRepository implements FeeInvoiceRepositoryInterface
{
    public function index()
    {
        $fee_invoices = FeeInvoice::all();
        $grades = Grade::all();
        return view('fees_invoices.index', compact('fee_invoices', 'grades'));
    }

    public function show($id)
    {
        $student = Student::findorfail($id);
        $fees = Fee::where('classroom_id', $student->classroom_id)->get();
        return view('fees_invoices.add', compact('student', 'fees'));
    }

    public function edit($id)
    {
        $fee_invoices = FeeInvoice::findorfail($id);
        $fees = Fee::where('classroom_id', $fee_invoices->classroom_id)->get();
        return view('fees_invoices.edit', compact('fee_invoices', 'fees'));
    }

    public function store($request)
    {
        $list_fees = $request->List_Fees;

        DB::beginTransaction();

        try {

            foreach ($list_fees as $list_fee) {
                // حفظ البيانات في جدول فواتير الرسوم الدراسية
                $fees = new FeeInvoice();
                $fees->invoice_date = date('Y-m-d');
                $fees->student_id = $list_fee['student_id'];
                $fees->grade_id = $request->grade_id;
                $fees->classroom_id = $request->classroom_id;;
                $fees->fee_id = $list_fee['fee_id'];
                $fees->amount = $list_fee['amount'];
                $fees->description = $list_fee['description'];
                $fees->save();

                // حفظ البيانات في جدول حسابات الطلاب
                $studentAccount = new studentAccount();
                $studentAccount->date = date('Y-m-d');
                $studentAccount->type = 'invoice';
                $studentAccount->fee_invoice_id = $fees->id;
                $studentAccount->student_id = $list_fee['student_id'];
                // $studentAccount->grade_id = $request->grade_id;
                // $studentAccount->classroom_id = $request->classroom_id;
                $studentAccount->debit = $list_fee['amount'];
                $studentAccount->credit = 0.00;
                $studentAccount->description = $list_fee['description'];
                $studentAccount->save();
            }

            DB::commit();

            toastr()->success(trans('messages.success'));
            return redirect()->route('fees_invoices.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function update($request)
    {
        DB::beginTransaction();
        try {
            // تعديل البيانات في جدول فواتير الرسوم الدراسية
            $Fees = FeeInvoice::findorfail($request->id);
            $Fees->fee_id = $request->fee_id;
            $Fees->amount = $request->amount;
            $Fees->description = $request->description;
            $Fees->save();

            // تعديل البيانات في جدول حسابات الطلاب
            $StudentAccount = StudentAccount::where('fee_invoice_id', $request->id)->first();
            $StudentAccount->Debit = $request->amount;
            $StudentAccount->description = $request->description;
            $StudentAccount->save();
            DB::commit();

            toastr()->success(trans('messages.update'));
            return redirect()->route('fees_invoices.index');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy($request)
    {
        try {
            FeeInvoice::destroy($request->id);
            toastr()->error(trans('messages.delete'));
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

}