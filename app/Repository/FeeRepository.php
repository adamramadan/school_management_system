<?php


namespace App\Repository;


use App\Models\Fee;
use App\Models\Grade;

class FeeRepository implements FeeRepositoryInterface
{

    public function index()
    {

        $fees = Fee::all();
        $grades = Grade::all();
        return view('fees.index', compact('fees', 'grades'));
    }

    public function create()
    {

        $grades = Grade::all();
        return view('fees.add', compact('grades'));
    }

    public function edit($id)
    {

        $fee = Fee::findorfail($id);
        $grades = Grade::all();
        return view('fees.edit', compact('fee', 'grades'));
    }


    public function store($request)
    {
        try {

            $fees = new Fee();
            $fees->title = ['en' => $request->title_en, 'ar' => $request->title_ar];
            $fees->amount  = $request->amount;
            $fees->grade_id  = $request->grade_id;
            $fees->classroom_id  = $request->classroom_id;
            $fees->description  = $request->description;
            $fees->year  = $request->year;
            $fees->fee_type  = $request->fee_type;

            $fees->save();
            toastr()->success(trans('messages.success'));
            return redirect()->route('fees.create');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function update($request)
    {
        try {
            $fees = Fee::findorfail($request->id);
            $fees->title = ['en' => $request->title_en, 'ar' => $request->title_ar];
            $fees->amount  = $request->amount;
            $fees->grade_id  = $request->grade_id;
            $fees->classroom_id  = $request->classroom_id;
            $fees->description  = $request->description;
            $fees->year  = $request->year;
            $fees->fee_type  = $request->fee_type;

            $fees->save();
            toastr()->success(trans('messages.update'));
            return redirect()->route('fees.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy($request)
    {
        try {
            Fee::destroy($request->id);
            toastr()->error(trans('messages.delete'));
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }
}
