<?php


namespace App\Repository;


interface StudentGraduatedRepositoryInterface
{

    public function index();

    public function create();

    // update Students to SoftDelete
    public function softDelete($request);

    // ReturnData Students
    public function returnData($request);

    // destroy Students
    public function destroy($request);
}
