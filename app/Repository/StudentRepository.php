<?php

namespace App\Repository;

use App\Models\BloodType;
use App\Models\Classroom;
use App\Models\Gender;
use App\Models\Grade;
use App\Models\Image;
use App\Models\MyParent;
use App\Models\Nationality;
use App\Models\Section;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StudentRepository implements StudentRepositoryInterface
{
    public function get_student()
    {
        $students = Student::all();
        return view('students.index_students', compact('students'));
    }

    public function edit_student($id)
    {
        $data['grades'] = Grade::all();
        $data['parents'] = MyParent::all();
        $data['genders'] = Gender::all();
        $data['nationalities'] = Nationality::all();
        $data['blood_types'] = BloodType::all();
        $students =  Student::findOrFail($id);
        return view('students.edit_student', $data, compact('students'));
    }

    public function update_student($request)
    {
        try {
            $edit_Student = Student::findorfail($request->id);
            $edit_Student->name = ['ar' => $request->name_ar, 'en' => $request->name_en];
            $edit_Student->email = $request->email;
            $edit_Student->password = Hash::make($request->password);
            $edit_Student->gender_id = $request->gender_id;
            $edit_Student->nationality_id = $request->nationality_id;
            $edit_Student->blood_id = $request->blood_id;
            $edit_Student->date_birth = $request->date_birth;
            $edit_Student->grade_id = $request->grade_id;
            $edit_Student->classroom_id = $request->classroom_id;
            $edit_Student->section_id = $request->section_id;
            $edit_Student->parent_id = $request->parent_id;
            $edit_Student->academic_year = $request->academic_year;
            $edit_Student->save();
            toastr()->success(trans('messages.update'));
            return redirect()->route('students.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function create_student()
    {

        $data['grades'] = Grade::all();
        $data['parents'] = MyParent::all();
        $data['genders'] = Gender::all();
        $data['nationalities'] = Nationality::all();
        $data['blood_types'] = BloodType::all();
        return view('students.add_student', $data);
    }

    public function show_student($id)
    {
        $student = Student::findorfail($id);
        return view('students.show_student', compact('student'));
    }

    public function get_classrooms($id)
    {

        $list_classes = Classroom::where("grade_id", $id)->pluck("name_class", "id");
        return $list_classes;
    }

    //Get Sections
    public function get_sections($id)
    {

        $list_sections = Section::where("classroom_id", $id)->pluck("name_section", "id");
        return $list_sections;
    }

    public function store_student($request)
    {
        DB::beginTransaction();

        try {
            $students = new Student();
            $students->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
            $students->email = $request->email;
            $students->password = Hash::make($request->password);
            $students->gender_id = $request->gender_id;
            $students->nationality_id = $request->nationality_id;
            $students->blood_id = $request->blood_id;
            $students->date_birth = $request->date_birth;
            $students->grade_id = $request->grade_id;
            $students->classroom_id = $request->classroom_id;
            $students->section_id = $request->section_id;
            $students->parent_id = $request->parent_id;
            $students->academic_year = $request->academic_year;
            $students->save();

            if ($request->hasfile('photos')) {
                foreach ($request->file('photos') as $file) {
                    $name = $file->getClientOriginalName();
                    $file->storeAs('attachments/students/' . $students->name, $file->getClientOriginalName(), 'upload_attachments');

                    // insert in image_table
                    $images = new Image();
                    $images->file_name = $name;
                    $images->imageable_id = $students->id;
                    $images->imageable_type = 'App\Models\Student';
                    $images->save();
                }
            }
            DB::commit();

            toastr()->success(trans('messages.success'));
            return redirect()->route('students.create');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function delete_student($request)
    {

        Student::destroy($request->id);
        toastr()->error(trans('messages.delete'));
        return redirect()->route('students.index');
    }


    public function upload_attachment($request)
    {
        foreach ($request->file('photos') as $file) {
            $name = $file->getClientOriginalName();
            $file->storeAs('attachments/students/' . $request->student_name, $file->getClientOriginalName(), 'upload_attachments');

            // insert in image_table
            $images = new image();
            $images->file_name = $name;
            $images->imageable_id = $request->student_id;
            $images->imageable_type = 'App\Models\Student';
            $images->save();
         
        }
        toastr()->success(trans('messages.success'));
        return redirect()->route('students.show', $request->student_id);
    }

    public function download_attachment($studentName, $fileName)
    {
        return response()->download(public_path('attachments/students/' . $studentName . '/' . $fileName));
    }

    public function delete_attachment($request)
    {
        // Delete img in server disk
        Storage::disk('upload_attachments')->delete('attachments/students/' . $request->student_name . '/' . $request->filename);

        // Delete in data
        image::where('id', $request->id)->where('file_name', $request->filename)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('students.show', $request->student_id);
    }
}
