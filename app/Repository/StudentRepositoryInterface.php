<?php

namespace App\Repository;

interface StudentRepositoryInterface
{

    //  Get_Student
    public function get_student();

    // Edit_Student
    public function edit_student($id);

    //Update_Student
    public function update_student($request);

    // Show_Student
    public function show_student($id);

    //Delete_Student
    public function delete_student($request);

    // Get Add Form Student
    public function create_student();

    // Get classrooms
    public function get_classrooms($id);

    //Get Sections
    public function get_sections($id);

    //Store_Student
    public function store_student($request);

    //Upload_attachment
    public function upload_attachment($request);

    //Download_attachment
    public function download_attachment($studentName, $fileName);

    //Delete_attachment
    public function delete_attachment($request);
}
