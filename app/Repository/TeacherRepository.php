<?php

namespace App\Repository;

use App\Models\Gender;
use App\Models\Specialization;
use App\Models\Teacher;
use Exception;
use Illuminate\Support\Facades\Hash;

class TeacherRepository implements TeacherRepositoryInterface
{

    public function getAllTeachers()
    {
        return Teacher::all();
    }
    public function getSpecialization()
    {
        return Specialization::all();
    }

    public function getGender()
    {
        return Gender::all();
    }

    public function storeTeacher($request)
    {

        try {
            $teachers = new Teacher();
            $teachers->email = $request->email;
            $teachers->password =  Hash::make($request->password);
            $teachers->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
            $teachers->specialization_id = $request->specialization_id;
            $teachers->gender_id = $request->gender_id;
            $teachers->joining_date = $request->joining_date;
            $teachers->address = $request->address;
            $teachers->save();
            toastr()->success(trans('messages.success'));
            return redirect()->route('teachers.create');
        } catch (Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function editTeacher($id)
    {
        return Teacher::findOrFail($id);
    }


    public function updateTeacher($request)
    {
        try {
            $teachers = Teacher::findOrFail($request->id);
            $teachers->email = $request->email;
            $teachers->password =  Hash::make($request->password);
            $teachers->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
            $teachers->specialization_id = $request->specialization_id;
            $teachers->gender_id = $request->gender_id;
            $teachers->joining_date = $request->joining_date;
            $teachers->address = $request->address;
            $teachers->save();
            
            toastr()->success(trans('messages.update'));
            return redirect()->route('teachers.index');
        } catch (Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }


    public function deleteTeacher($request)
    {
        Teacher::findOrFail($request->id)->delete();
        toastr()->error(trans('messages.delete'));
        return redirect()->route('teachers.index');
    }
}
