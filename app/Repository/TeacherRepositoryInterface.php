<?php

namespace App\Repository;

interface TeacherRepositoryInterface
{

    // get all Teachers
    public function getAllTeachers();


    public function getSpecialization();

    // Get Gender
    public function getGender();

    // StoreTeachers
    public function storeTeacher($request);

    //editTeacher

    public function editTeacher($id);

    //updateTeacher

    public function updateTeacher($request);

    // deleteTeacher

    public function deleteTeacher($request);
}
