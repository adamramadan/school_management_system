<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id');

            $table->unsignedBigInteger('from_grade');
            $table->unsignedBigInteger('from_classroom');
            $table->unsignedBigInteger('from_section');
            $table->unsignedBigInteger('to_grade');
            $table->unsignedBigInteger('to_classroom');
            $table->unsignedBigInteger('to_section');
            $table->string('old_academic_year');
            $table->string('new_academic_year');
            $table->foreign('student_id')->references('id')->on('students')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('from_grade')->references('id')->on('grades')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('from_classroom')->references('id')->on('classrooms')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('from_section')->references('id')->on('sections')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to_grade')->references('id')->on('grades')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to_classroom')->references('id')->on('classrooms')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to_section')->references('id')->on('sections')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
