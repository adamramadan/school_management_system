<?php

return [
    'student_attendance_and_absence_list' => 'قائمة الحضور والغياب للطلاب',
    'date' => 'تاريخ اليوم',
    'presence' => 'حضور',
    'absence' => 'غياب',
    'attendance' => 'الحضور والغياب',
    'student_list' => 'قائمة الطلاب',
];
