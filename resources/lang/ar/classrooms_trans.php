<?php

return [

  'title_page' => 'الصفوف الدراسية',
  'classes_list' => 'قائمة الصفوف الدراسية',
  'add_class' => 'إضافة صف',
  'edit_class'=> 'تعديل صف',
  'delete_class'=> 'حذف صف',
  'warning_class'=> 'هل انت متاكد من عملية الحذف ؟',
  'name_class_ar' => 'إسم الصف بالعربية',
//   'name_class' => 'إسم الصف بالإنجليزية',
  'submit' => 'حفظ البيانات',
  'name_class'=>'إسم الصف ',
  'name_class_en'=>'إسم الصف بالانجلزية',
  'name_grade'=>'إسم المرحلة',
  'add_row'=>'إدراج سجل',
  'delete_row'=>'حذف',
  'processes'=>'العمليات',
  'edit'=>'تعديل',
  'delete'=>'حذف',
  'close' => 'إغلاق',
  'delete_checkbox'=> 'حذف الصفوف المحددة',
  'search_by_grade'=> 'بحث بإسم المرحلة',

];