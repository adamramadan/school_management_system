<?php
return [
    'add_new_invoice' => 'إضافة فاتورة جديدة',
    'student_name' => 'إسم الطالب',
    'choose_from_the_list' => 'إختر من القائمة ',
    'statement' => 'البيان',
    'study_invoice' => 'الفاتورة الدراسية',
    'invoices' => 'الفواتير',
    'delete_invoice' => 'حذف فاتورة',
    'edit_study_fee' =>'تعديل رسوم دراسية',
    'add_fee_invoice' => 'إضافة فاتورة رسوم',
    'receipt' => 'سند قبض',
    'receipts' => 'سندات القبض',
    'edit_receipt' => 'تعديل سند قبض',
    'delete_receipt' => 'حذف سند قبض',



];
