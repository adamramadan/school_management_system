<?php
return [
    'study_fees' => 'الرسوم الدراسية',
    'add_new_fees' => 'إضافة رسوم جديدة',
    'name' => 'الإسم',
    'amount' => 'المبلغ',
    'grade' => 'المرحلة الدراسية',
    'classroom' => 'الصف الدراسي',
    'academic_year' => 'السنة الدراسية',
    'details' => 'تفاصيل',
    'processes' => 'العمليات',
    'name_ar' => 'الإسم باللغة العربية',
    'name_en' => 'الإسم باللغة الإنجليزية',
    'confirm' => 'تأكيد',
    'edit_study_fees' => 'تعديل الرسوم الدراسية',
    'delete_sure' => 'هل أنت متاكد من عملية الحذف ؟',
    'fee_type' => 'نوع الرسوم',
    'study_fees#' => 'رسوم دراسية',
    'fees_bus' => 'رسوم باص',
    'study_fee_refund' => 'إسترداد الرسوم الدراسية',
    'fee_exclusion' => 'إستبعاد رسوم',
    'student_fee' => 'رسوم  الطالب',
    'edit_study_fee_refund' => 'تعديل إسترداد الرسوم الدراسية ',
    'cashing' => 'سند صرف',
    'cashing#' => 'سندات الصرف',
    'edit_cashing' => 'تعديل سند صرف',
    'delete_cashing' => 'حذف سند صرف',

    

];
