<?php

return [
    'change_language' => 'اللغة',
    'dashboard' => 'لوحة التحكم',
    'dashboard_page' => 'لوحة التحكم',
    'programmer' => 'نظام إدارة المدرسة',
    'grades' => 'المراحل الدراسية',
    'grades_list' => 'قائمة المراحل الدراسية',
    'classes' => 'الصفوف',
    'classes_list' => 'قائمة الصفوف الدراسية',
    'sections' => 'الأقسام',
    'sections_list' => 'قائمة الأقسام',
    'parents' => 'أولياء الأمور',
    'parents_list' => 'قائمة أولياء الأمور',
    'add_parent' => 'إضافة ولي أمر',
    'teachers' => 'المعلمين',
    'teachers_list' => 'قائمة المعلمين',
    'students' => 'الطلاب',
    'add_student' => 'إضافة طالب جديد',
    'students_list' => 'قائمة الطلاب',
    'students_promotion' => 'ترقية الطلاب',
    'students_information' => 'معلومات الطلاب',
    'add_promotion' => 'إضافة ترقية',
    'promotions_list' => 'إدارة الترقيات',
    'graduating_students' => 'الطلاب المتخرجين',
    'add_graduate' => 'إضافة تخرج جديد',
    'graduate_list' => 'قائمة المتخرجين',
    'accounts' => 'الحسابات',
    'study_fees' => 'الرسوم الدراسية',
    'attendance' => 'الحضور',



];
