<?php

return [

    'step1' => 'معلومات الأب',
    'step2' => 'معلومات الأم',
    'step3' => 'تاكيد المعلومات',
    'email' => 'البريد الالكتروني',
    //معلومات الأب
    'password' => 'كلمة المرور',
    'father_name' => 'إسم الأب باللغة العربية',
    'father_name_en' => 'إسم الأب  باللغة الإنجليزية',
    'father_job' => 'إسم الوظيفة باللغة العربية',
    'father_job_en' => 'إسم الوظيفة باللغة الإنجليزية',
    'father_id_number' => 'رقم الهوية',
    'father_id_passport' => 'رقم جواز السفر',
    'father_phone' => 'رقم الهاتف',
    'father_nationality_id' => 'الجنسية',
    'father_blood_type_id' => 'فصلية الدم',
    'father_religion_id' => 'الديانة',
    'father_address' => 'عنوان الأب',

    //معلومات الأم
    'mother_name' => 'إسم الأم باللغة العربية',
    'mother_name_en' => 'إسم الأم  باللغة الإنجليزية',
    'mother_job' => 'إسم الوظيفة باللغة العربية',
    'mother_job_en' => 'إسم الوظيفة باللغة الإنجليزية',
    'mother_id_number' => 'رقم الهوية',
    'mother_id_passport' => 'رقم جواز السفر',
    'mother_phone' => 'رقم الهاتف',
    'mother_nationality_id' => 'الجنسية',
    'mother_blood_type_id' => 'فصلية الدم',
    'mother_religion_id' => 'الديانة',
    'mother_address' => 'عنوان الأم',

    'next' => 'التالي',
    'back' => 'السابق',
    'finish' => 'تاكيد',
    'choose' => 'اختيار من القائمة',
    'attachments' => 'المرفقات',
    'processes' => 'العمليات',




];
