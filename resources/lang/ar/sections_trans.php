<?php

return [

    'title_page' => 'الأقسام',
    'list_grade' => 'List_Grade',
    'add_section' => 'إضافة قسم',
    'edit_section'=> 'تعديل قسم',
    'delete_section'=> 'حذف قسم',
    'warning_section'=> 'هل انت متاكد من عملية الحذف',
    'section_name_ar' => 'إسم القسم باللغة العربية',
    'section_name_en' => 'إسم القسم باللغة الإنجليزية',
    'select_grade' => '-- حدد المرحلة --',
    'name_grade' => 'إسم المرحلة',
    'notes' => 'Notes',
    'submit' => 'حفظ البيانات',
    'name_section'=>'إسم القسم',
    'name_class'=>'إسم الصف',
    'status'=>'الحالة',
    'status_Section_AC'=>'نشط',
    'status_Section_No'=>'غير نشط',
    'delete_grade_Error'=>'The Grade cannot be deleted because there are classes attached to it',
    'required_ar'=>'يرجى إدخال إسم القسم باللغة العربية',
    'required_en'=>'يرجى إدخال إسم القسم باللغة الإنجليزية',
    'grade_id_required'=>'يرجى اختيار إسم المرحلة',
    'class_id_required'=>'يرجى تحديد الصف الدراسي',
    'processes'=>'العمليات',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'close' => 'إغلاق',

];