<?php

return [
    'subjects' => 'المواد الدراسية',
    'subject_list' => 'قائمة المواد الدراسية',
    'add_a_subject' => 'إضافة مادة دراسية',
    'name_of_the_subject_ar' => 'اسم المادة بالعربية',
    'name_of_the_subject_en' => 'اسم المادة بالإنجليزية',
    'name_of_the_subject' => 'اسم المادة الدراسية',
    'delete_subject' => 'حذف مادة دراسية',


];
