<?php

return [
    'teacher_name' => 'إسم المعلم',
    'add_teacher' => 'إضافة معلم',
    'edit_teacher' => 'تعديل معلم',
    'delete_teacher' => 'حذف معلم',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'name_ar' => 'إسم المعلم باللغة العربية',
    'name_en' => 'إسم المعلم باللغة الإنجليزية',
    'specialization' => 'التخصص',
    'gender' => 'النوع',
    'joining_date' => 'تاريخ التعين',
    'address' => 'العنوان',
    'processes' => 'العمليات'

];
