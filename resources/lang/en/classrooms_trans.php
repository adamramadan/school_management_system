<?php

return [

    'title_page' => 'Classes',
    'classes_list' => 'List Classes',
    'add_class' => 'add class',
    'edit_class'=> 'تعديل صف',
    'delete_class'=> 'حذف صف',
    'warning_class'=> 'Are You Sure of the Deleting Process ?',
    'submit' => 'submit',
    'name_class'=>'Class Name',
    'name_class_ar'=>'Class Name in Arabic',
    'name_class_en'=>'Class Name in English',
    'name_grade'=>'Name Grade',
    'add_row'=>'add row',
    'delete_row'=>'Delete row',
    'processes'=>'Processes',
    'edit'=>'Edit',
    'delete'=>'Delete',
    'close' => 'Close',
    'delete_checkbox'=> 'Delete Selected',
    'search_By_grade'=> 'Search by Grade',





];