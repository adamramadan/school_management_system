<?php
return [
    'add_new_invoice' => 'Add a new invoice',
    'student_name' => 'Student Name',
    'choose_from_the_list' => 'Choose from the list',
    'statement' => 'Statement',
    'study_invoice' => 'Study Invoice',
    'invoices' => 'Invoices',
    'delete_invoice' => 'Delete Invoice',
    'edit_study_fee' => 'Edit Study Fee',
    'add_fee_invoice' => 'Add a Fee Invoice',
    'receipt' => 'Receipt',
    'receipts' => 'Receipts',
    'edit_receipt' => 'Edit Receipt',
    'delete_receipt' =>'Delete Receipt'
];
