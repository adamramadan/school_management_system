<?php
return [
    'study_fees' => 'Study Fees',
    'add_new_fees' => 'Add New Fees',
    'name' => 'Name',
    'amount' => 'Amount',
    'grade' => 'Grade',
    'classroom' => 'Classroom',
    'academic_year' => 'Academic Year',
    'details' => 'Details',
    'processes' => 'Processes',
    'name_ar' => 'Name is in Arabic',
    'name_en' => 'Name is in English',
    'confirm' => 'confirm',
    'edit_study_fees' => 'Edit Study Fees',
    'delete_sure' => 'Are Sure of the Deleting Process ?',
    'fee_type' => 'Fee Type',
    'study_fees#' => 'Study Fees',
    'fees_bus' => 'Study Bus',
    'study_fee_refund' => 'Study Fee Refund',
    'fee_exclusion' => 'Fee Exclusion',
    'student_fee' => 'Student Fee',
    'edit_study_fee_refund' => 'Edit Study Fee Refund',
    'cashing' => 'Cashing',
    'cashing#' => 'Cashing',
    'edit_cashing' => 'Edit Cashing',
    'delete_cashing' => 'Delete Cashing',




];
