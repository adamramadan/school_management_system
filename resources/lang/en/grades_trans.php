<?php

return [
    'title_page' => 'Grades',
    'grades_list' => 'List Grades',
    'add_grade' => 'Add Grade',
    'edit_grade'=> 'Edit Grade',
    'delete_grade'=> 'Delete Grade',
    'warning_grade'=> 'Are Sure Of The Deleting Process ?',
    'stage_name_ar' => 'Stage name in Arabic',
    'stage_name_en' => 'Stage Name in English',
    'notes' => 'Notes',
    'submit' => 'submit',
    'name'=>'Name',
    'processes'=>'Processes',
    'delete_grade_error'=>'The Grade cannot be deleted because there are classes attached to it',
    'edit'=>'Edit',
    'exists'=>'This field already exists',
    'delete'=>'Delete',
    'close' => 'Close',

];
