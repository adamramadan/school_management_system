<?php

return [
    'change_language' => 'language',
    'dashboard' => 'Dashboard',
    'dashboard_page' => 'Dashboard',
    'programmer' => 'School Management System',
    'grades' => 'Grades',
    'grades_list' => 'List Grades',
    'classes' => 'Classroom',
    'classes_list' => 'Classes List',
    'sections' => 'Sections',
    'sections_list' => 'Sections List',
    'parents' => 'Parents',
    'parents_list' => 'Parents List',
    'add_parent' => 'Add Parents',
    'teachers' => 'Teachers',
    'teachers_list' => 'Teacher List',
    'students' => 'Students',
    'add_student' => 'Add Student',
    'students_list' => 'Students List',
    'name' => 'Name',
    'edit_student' => ' Edit Student',
    'students_promotion' => 'Students Promotion',
    'students_information' => 'Students Information',
    'add_promotion' => 'Add Promotion',
    'promotions_list' => 'إدارة الترقيات',
    'promotions_list' => 'Promotions Management',
    'graduating_students' => 'Graduating List',
    'add_graduate' => 'Add Graduate',
    'graduate_list' => 'Graduate List',
    'study_fees' => 'Study Fees',
    'accounts' => 'Accounts',
    'attendance' => 'Attendance',





];
