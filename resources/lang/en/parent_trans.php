<?php

return [

    'step1' => 'Father information',
    'step2' => 'Mother information',
    'step3' => 'Confirm information',
    'email' => 'Email',
    'password' => 'Password',
    'father_name' => 'Father Name Arabic',
    'father_name_ne' => 'Father Name English',
    'father_job' => 'Job Title Arabic',
    'father_job_en' => 'Job Title English',
    'father_id_number' => 'Identification Number',
    'father_id_passport' => 'Passport Number',
    'father_phone' => 'Phone Number',
    'father_nationality_id' => 'Nationality',
    'father_blood_type_id' => 'Blood Type',
    'father_religion_id' => 'Religion',
    'father_address' => 'Father Address',

    'next' => 'Next',
    'choose' => 'Choose',

    //معلومات الام
    'mother_name' => 'Mother Name Arabic',
    'mother_name_en' => 'Mother Name English',
    'mother_job' => 'Job Title Arabic',
    'mother_job_en' => 'Job Title English',
    'mother_id_number' => 'Identification Number',
    'mother_id_passport' => 'Passport Number',
    'mother_phone' => 'Phone Number',
    'mother_nationality_id' => 'Nationality',
    'mother_blood_type_id' => 'Blood Type',
    'mother_religion_id' => 'Religion',
    'mother_address' => ' Mother Address',

    'next' => 'Next',
    'back' => 'Previous',
    'finish' => 'Confirm',
    'choose' => 'Choose From The List',
    'attachments' => 'Attachments',
    'processes' => 'Processes',
    

];
