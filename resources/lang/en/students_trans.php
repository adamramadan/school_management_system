<?php

return [

    'personal_information' => 'Personal Information',
    'name_ar' => 'Name Ar',
    'name_en' => 'Name En',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'gender' => 'Gender',
    'nationality' => 'Nationality',
    'blood_type' => 'Blood Type',
    'date_of_birth' => 'Date of Birth',
    'student_information' => 'Student Information',
    'grade' => 'Grade',
    'classrooms' => 'Classrooms',
    'section' => 'Section',
    'parent' => 'Parent',
    'academic_year' => 'Academic Year',
    'submit' => 'Submit',
    'processes' => 'Processes',
    'personal_information' => 'Personal Information',
    'submit' => 'submit',
    'close' => 'Close',
    'deleted_student' => 'Delete student data',
    'deleted_student_tilte' => 'Are you sure to delete the student?',
    'attachments' => 'Attachments',
    'student_details' => 'Student Details',
    'filename' => 'File Name',
    'created_at' => 'created at',
    'download' => 'Download',
    'delete' => 'Delete',
    'delete_attachment' => 'Delete Attachment',
    'delete_attachment_tilte' => 'Are you sure to delete the attachment?',
    'old_school_grade' => 'Previous School Grade',
    'new_school_grade' => 'New School Grade',
    'confirm' => 'Confirm',
    'back_to_all' => 'Back To All',
    'old_academic_year' => 'Previous School Year',
    'old_class' => 'Previous Class',
    'old_section' => 'Previous Section',
    'current_grade_level' => 'Current Grade Level',
    'current_school_year' => 'Current School Year',
    'current_class' => 'Current Class',
    'current_section' => 'Current Section',
    'sure_back_to_all' => 'Are You Sure  the Process of Reviewing all Students ?',
    'sure_back_to_one' => 'Are You Sure Of The Proccess Of Revwiewing Student ?  ',
    'back_to_student' => 'Back To Student',
    'confirm' => 'Confirm',
    'yes' => 'Yes',
    'return_student' => 'Return Student',
    'delete_student' => 'Delete Student',
    'deleted_student_m' => 'Are You Sure of the Deleting Process ?',
    'cancel_graduation' => 'Are You Sure of Cancel Graduation Process ?',
    'student_list' => 'Student List'






];
