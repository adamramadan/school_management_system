<?php

return [
    'subjects' => 'Subjects',
    'subject_list' => 'Subject List',
    'add_a_subject' => 'Add Subject',
    'name_of_the_subject_ar' => 'Name of the Subject AR',
    'name_of_the_subject_en' => 'Name of the Subject EN',
    'name_of_the_subject' => 'Name of the Subject',
    'delete_subject' => 'Delete a Subject',



];
