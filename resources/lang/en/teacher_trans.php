<?php

return [
    'teacher_name' => 'Name Teacher',
    'add_teacher' => 'Add Teacher',
    'edit_teacher' => 'Edit Teacher',
    'delete_teacher' => 'Delete Teacher',
    'email' => 'Email',
    'password' => 'Password',
    'name_ar' => 'Name Ar',
    'name_en' => 'Name En',
    'specialization' => 'Specialization',
    'gender' => 'Gender',
    'joining_date' => 'Joining Date',
    'ddress' => 'Address',
    'processes' => 'Processes'

];
