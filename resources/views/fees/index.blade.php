@extends('layouts.master')
@section('css')
    @toastr_css
@section('title')
    {{ trans('fees_trans.study_fees') }}
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
@section('PageTitle')
    {{ trans('fees_trans.study_fees') }}
@stop
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">
                <div class="col-xl-12 mb-30">
                    <div class="card card-statistics h-100">
                        <div class="card-body">
                            <a href="{{ route('fees.create') }}" class="btn btn-success btn-sm" role="button"
                                aria-pressed="true">{{ trans('fees_trans.add_new_fees') }}</a><br><br>
                            <div class="table-responsive">
                                <table id="datatable" class="table  table-hover table-sm table-bordered p-0"
                                    data-page-length="50" style="text-align: center">
                                    <thead>
                                        <tr class="alert-success">
                                            <th>#</th>
                                            <th>{{ trans('fees_trans.name') }}</th>
                                            <th>{{ trans('fees_trans.amount') }}</th>
                                            <th>{{ trans('fees_trans.grade') }}</th>
                                            <th>{{ trans('fees_trans.classroom') }}</th>
                                            <th>{{ trans('fees_trans.academic_year') }}</th>
                                            <th>{{ trans('fees_trans.details') }}</th>
                                            <th>{{ trans('fees_trans.processes') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($fees as $fee)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $fee->title }}</td>
                                                <td>{{ number_format($fee->amount, 2) }}</td>
                                                <td>{{ $fee->grade->name }}</td>
                                                <td>{{ $fee->classroom->name_class }}</td>
                                                <td>{{ $fee->year }}</td>
                                                <td>{{ $fee->description }}</td>
                                                <td>
                                                    <a href="{{ route('fees.edit', $fee->id) }}"
                                                        class="btn btn-info btn-sm" role="button" aria-pressed="true"><i
                                                            class="fa fa-edit"></i></a>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#Delete_Fee{{ $fee->id }}"
                                                        title="{{ trans('grades_trans.delete') }}"><i
                                                            class="fa fa-trash"></i></button>
                                                    <a href="#" class="btn btn-warning btn-sm" role="button"
                                                        aria-pressed="true"><i class="far fa-eye"></i></a>

                                                </td>
                                            </tr>
                                            @include('fees.delete')
                                        @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
@endsection
@section('js')
@toastr_js
@toastr_render
@endsection
