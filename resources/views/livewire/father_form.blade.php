@if ($currentStep != 1)
    <div style="display: none" class="row setup-content" id="step-1">
@endif
<div class="col-xs-12">
    <div class="col-md-12">
        <br>
        <div class="form-row">
            <div class="col">
                <label for="title">{{ trans('parent_trans.email') }}</label>
                <input type="email" wire:model="email" class="form-control">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col">
                <label for="title">{{ trans('parent_trans.password') }}</label>
                <input type="password" wire:model="password" class="form-control">
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label for="title">{{ trans('parent_trans.father_name') }}</label>
                <input type="text" wire:model="father_name" class="form-control">
                @error('father_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col">
                <label for="title">{{ trans('parent_trans.father_name_en') }}</label>
                <input type="text" wire:model="father_name_en" class="form-control">
                @error('father_name_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-3">
                <label for="title">{{ trans('parent_trans.father_job') }}</label>
                <input type="text" wire:model="father_job" class="form-control">
                @error('father_job')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-3">
                <label for="title">{{ trans('parent_trans.father_job_en') }}</label>
                <input type="text" wire:model="father_job_en" class="form-control">
                @error('father_job_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="col">
                <label for="title">{{ trans('parent_trans.father_id_number') }}</label>
                <input type="text" wire:model="father_id_number" class="form-control">
                @error('father_id_number')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col">
                <label for="title">{{ trans('parent_trans.father_id_passport') }}</label>
                <input type="text" wire:model="father_id_passport" class="form-control">
                @error('father_id_passport')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="col">
                <label for="title">{{ trans('parent_trans.father_phone') }}</label>
                <input type="text" wire:model="father_phone" class="form-control">
                @error('father_phone')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">{{ trans('parent_trans.father_nationality_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="father_nationality_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($nationalities as $nationality)
                        <option value="{{ $nationality->id }}">{{ $nationality->name }}</option>
                    @endforeach
                </select>
                @error('father_nationality_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col">
                <label for="inputState">{{ trans('parent_trans.father_blood_type_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="father_blood_type_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($type_bloods as $type_blood)
                        <option value="{{ $type_blood->id }}">{{ $type_blood->name }}</option>
                    @endforeach
                </select>
                @error('father_blood_type_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col">
                <label for="inputZip">{{ trans('parent_trans.father_religion_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="father_religion_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($religions as $religion)
                        <option value="{{ $religion->id }}">{{ $religion->name }}</option>
                    @endforeach
                </select>
                @error('father_religion_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>


        <div class="form-group">
            <label for="exampleFormControlTextarea1">{{ trans('parent_trans.father_address') }}</label>
            <textarea class="form-control" wire:model="father_address" id="exampleFormControlTextarea1"
                rows="4"></textarea>
            @error('father_address')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>


        @if ($updateMode)
            <button class="btn btn-success btn-sm nextBtn btn-lg pull-right" wire:click="firstStepSubmitEdit"
                type="button">{{ trans('parent_trans.next') }}
            </button>
        @else
            <button class="btn btn-success btn-sm nextBtn btn-lg pull-right" wire:click="firstStepSubmit"
                type="button">{{ trans('parent_trans.next') }}
            </button>
        @endif

    </div>
</div>
</div>
