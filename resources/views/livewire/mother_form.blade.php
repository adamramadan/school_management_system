@if ($currentStep != 2)
    <div style="display: none" class="row setup-content" id="step-2">
@endif
<div class="col-xs-12">
    <div class="col-md-12">
        <br>

        <div class="form-row">
            <div class="col">
                <label for="title">{{ trans('parent_trans.mother_name') }}</label>
                <input type="text" wire:model="mother_name" class="form-control">
                @error('mother_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col">
                <label for="title">{{ trans('parent_trans.mother_name_en') }}</label>
                <input type="text" wire:model="mother_name_en" class="form-control">
                @error('mother_name_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-3">
                <label for="title">{{ trans('parent_trans.mother_job') }}</label>
                <input type="text" wire:model="mother_job" class="form-control">
                @error('mother_job')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-3">
                <label for="title">{{ trans('parent_trans.mother_job_en') }}</label>
                <input type="text" wire:model="mother_job_en" class="form-control">
                @error('mother_job_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="col">
                <label for="title">{{ trans('parent_trans.mother_id_number') }}</label>
                <input type="text" wire:model="mother_id_number" class="form-control">
                @error('mother_id_number')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col">
                <label for="title">{{ trans('parent_trans.mother_id_passport') }}</label>
                <input type="text" wire:model="mother_id_passport" class="form-control">
                @error('mother_id_passport')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="col">
                <label for="title">{{ trans('parent_trans.mother_phone') }}</label>
                <input type="text" wire:model="mother_phone" class="form-control">
                @error('mother_phone')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">{{ trans('parent_trans.mother_nationality_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="mother_nationality_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($nationalities as $nationality)
                        <option value="{{ $nationality->id }}">{{ $nationality->name }}</option>
                    @endforeach
                </select>
                @error('mother_nationality_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col">
                <label for="inputState">{{ trans('parent_trans.mother_blood_type_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="mother_blood_type_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($type_bloods as $type_blood)
                        <option value="{{ $type_blood->id }}">{{ $type_blood->name }}</option>
                    @endforeach
                </select>
                @error('mother_blood_type_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col">
                <label for="inputZip">{{ trans('parent_trans.mother_religion_id') }}</label>
                <select class="custom-select my-1 mr-sm-2" wire:model="mother_religion_id">
                    <option selected>{{ trans('parent_trans.choose') }}...</option>
                    @foreach ($religions as $religion)
                        <option value="{{ $religion->id }}">{{ $religion->name }}</option>
                    @endforeach
                </select>
                @error('mother_religion_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">{{ trans('parent_trans.mother_address') }}</label>
            <textarea class="form-control" wire:model="mother_address" id="exampleFormControlTextarea1"
                rows="4"></textarea>
            @error('mother_address')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <button class="btn btn-danger btn-sm nextBtn btn-lg pull-right" type="button" wire:click="back(1)">
            {{ trans('parent_trans.back') }}

        </button>
        @if ($updateMode)
            <button class="btn btn-success btn-sm nextBtn btn-lg pull-right" wire:click="secondStepSubmitEdit"
                type="button">{{ trans('parent_trans.next') }}
            </button>
        @else
            <button class="btn btn-success btn-sm nextBtn btn-lg pull-right" type="button"
                wire:click="secondStepSubmit">{{ trans('parent_trans.next') }}</button>
        @endif
    </div>
</div>
</div>
