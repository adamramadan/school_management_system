<button class="btn btn-success btn-sm btn-lg pull-right" wire:click="showformadd"
    type="button">{{ trans('main_trans.add_parent') }}</button><br><br>
<div class="table-responsive">
    <table id="datatable" class="table  table-hover table-sm table-bordered p-0" data-page-length="50"
        style="text-align: center">
        <thead>
            <tr class="table-success">
                <th>#</th>

                <th>{{ trans('parent_trans.email') }}</th>
                <th>{{ trans('parent_trans.father_name') }}</th>
                <th>{{ trans('parent_trans.father_id_number') }}</th>
                <th>{{ trans('parent_trans.father_id_passport') }}</th>
                <th>{{ trans('parent_trans.father_phone') }}</th>
                <th>{{ trans('parent_trans.father_job') }}</th>
                <th>{{ trans('parent_trans.processes') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; ?>
            @foreach ($my_parents as $my_parent)
                <tr>
                    <?php $i++; ?>
                    <td>{{ $i }}</td>
                    <td>{{ $my_parent->email }}</td>
                    <td>{{ $my_parent->father_name }}</td>
                    <td>{{ $my_parent->father_id_number }}</td>
                    <td>{{ $my_parent->father_id_passport }}</td>
                    <td>{{ $my_parent->father_phone }}</td>
                    <td>{{ $my_parent->father_job }}</td>
                    <td>
                        <button wire:click="edit({{ $my_parent->id }})" title="{{ trans('grades_trans.edit') }}"
                            class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                        <button type="button" class="btn btn-danger btn-sm" wire:click="delete({{ $my_parent->id }})"
                            title="{{ trans('grades_trans.delete') }}"><i class="fa fa-trash"></i></button>

                    </td>
                </tr>
            @endforeach
    </table>
</div>
