@extends('layouts.master')
@section('css')
    @toastr_css
@section('title')
    {{ trans('main_trans.students_list') }}
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
@section('PageTitle')
    {{ trans('main_trans.students_list') }}
@stop
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">
                <div class="col-xl-12 mb-30">
                    <div class="card card-statistics h-100">
                        <div class="card-body">
                            <a href="{{ route('students.create') }}" class="btn btn-success btn-sm" role="button"
                                aria-pressed="true">{{ trans('main_trans.add_student') }}</a><br><br>
                            <div class="table-responsive">
                                <table id="datatable" class="table  table-hover table-sm table-bordered p-0"
                                    data-page-length="50" style="text-align: center">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('students_trans.name') }}</th>
                                            <th>{{ trans('students_trans.email') }}</th>
                                            <th>{{ trans('students_trans.gender') }}</th>
                                            <th>{{ trans('students_trans.grade') }}</th>
                                            <th>{{ trans('students_trans.classrooms') }}</th>
                                            <th>{{ trans('students_trans.section') }}</th>
                                            <th>{{ trans('students_trans.processes') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($students as $student)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $student->name }}</td>
                                                <td>{{ $student->email }}</td>
                                                <td>{{ $student->gender->name }}</td>
                                                <td>{{ $student->grade->name }}</td>
                                                <td>{{ $student->classroom->name_class }}</td>
                                                <td>{{ $student->section->name_section }}</td>
                                                {{-- <td>
                                                    <a href="{{ route('students.edit', $student->id) }}"
                                                        class="btn btn-info btn-sm" role="button" aria-pressed="true"><i
                                                            class="fa fa-edit"></i></a>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                        data-toggle="modal"
                                                        data-target="#Delete_Student{{ $student->id }}"
                                                        title="{{ trans('grades_trans.delete') }}"><i
                                                            class="fa fa-trash"></i></button>
                                                    <a href="{{ route('students.show', $student->id) }}"
                                                        class="btn btn-warning btn-sm" role="button"
                                                        aria-pressed="true"><i class="far fa-eye"></i></a>
                                                </td> --}}




                                                <td>
                                                    <div class="dropdown show">
                                                        <a class="btn btn-success btn-sm dropdown-toggle" href="#"
                                                            role="button" id="dropdownMenuLink" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            {{ trans('students_trans.processes') }}
                                                        </a>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <a class="dropdown-item"
                                                                href="{{ route('students.show', $student->id) }}"><i
                                                                    style="color: #ffc107"
                                                                    class="far fa-eye "></i>&nbsp;
                                                                {{ trans('students_trans.personal_information') }}</a>
                                                            <a class="dropdown-item"
                                                                href="{{ route('students.edit', $student->id) }}"><i
                                                                    style="color:green"
                                                                    class="fa fa-edit"></i>&nbsp;
                                                                {{ trans('students_trans.edit_student') }}</a>
                                                            <a class="dropdown-item"
                                                                href="{{ route('fees_invoices.show', $student->id) }}"><i
                                                                    style="color: #0000cc"
                                                                    class="fa fa-edit"></i>&nbsp;
                                                                {{ trans('fees_invoices_trans.add_fee_invoice') }}&nbsp;</a>

                                                            <a class="dropdown-item"
                                                                href="{{ route('receipt_students.show', $student->id) }}"><i
                                                                    style="color: #9dc8e2"
                                                                    class="fas fa-money-bill-alt"></i>&nbsp; &nbsp;
                                                                {{ trans('fees_invoices_trans.receipt') }}</a>

                                                            <a class="dropdown-item"
                                                                href="{{ route('processing_fees.show', $student->id) }}"><i
                                                                    style="color: #9dc8e2"
                                                                    class="fas fa-money-bill-alt"></i>&nbsp; &nbsp;
                                                                {{ trans('fees_trans.fee_exclusion') }}</a>


                                                            <a class="dropdown-item"
                                                                href="{{ route('payment_students.show', $student->id) }}"><i
                                                                    style="color:goldenrod"
                                                                    class="fas fa-donate"></i>&nbsp;
                                                                &nbsp;{{ trans('fees_trans.cashing') }}</a>



                                                            <a class="dropdown-item"
                                                                data-target="#Delete_Student{{ $student->id }}"
                                                                data-toggle="modal"
                                                                href="##Delete_Student{{ $student->id }}"><i
                                                                    style="color: red" class="fa fa-trash"></i>&nbsp;
                                                                {{ trans('students_trans.deleted_student') }}</a>
                                                        </div>
                                                    </div>
                                                </td>





                                            </tr>
                                            @include('students.delete_student')
                                        @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
@endsection
@section('js')
@toastr_js
@toastr_render
@endsection
