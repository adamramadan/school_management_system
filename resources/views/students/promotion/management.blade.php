@extends('layouts.master')
@section('css')
    @toastr_css
@section('title')
    {{ trans('main_trans.students_list') }}
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
@section('PageTitle')
    {{ trans('main_trans.students_list') }}
@stop
<!-- breadcrumb -->
@endsection
@section('content')
<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">
                <div class="col-xl-12 mb-30">
                    <div class="card card-statistics h-100">
                        <div class="card-body">

                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Delete_all">
                                {{ trans('students_trans.back_to_all') }}
                            </button>
                            <br><br>


                            <div class="table-responsive">
                                <table id="datatable" class="table  table-hover table-sm table-bordered p-0"
                                    data-page-length="50" style="text-align: center">
                                    <thead>
                                        <tr>
                                            <th class="alert-info">#</th>
                                            <th class="alert-info">{{ trans('Students_trans.name') }}</th>
                                            <th class="alert-danger">{{ trans('students_trans.old_school_grade') }}
                                            </th>
                                            <th class="alert-danger">{{ trans('students_trans.old_academic_year') }}
                                            </th>
                                            <th class="alert-danger">{{ trans('students_trans.old_class') }}</th>
                                            <th class="alert-danger">{{ trans('students_trans.old_section') }}</th>
                                            <th class="alert-success">
                                                {{ trans('students_trans.current_grade_level') }}</th>
                                            <th class="alert-success">
                                                {{ trans('students_trans.current_school_year') }}</th>
                                            <th class="alert-success">{{ trans('students_trans.current_class') }}
                                            </th>
                                            <th class="alert-success">{{ trans('students_trans.current_section') }}
                                            </th>
                                            <th>{{ trans('students_trans.processes') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($promotions as $promotion)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $promotion->student->name }}</td>
                                                <td>{{ $promotion->f_grade->name }}</td>
                                                <td>{{ $promotion->old_academic_year }}</td>
                                                <td>{{ $promotion->f_classroom->name_class }}</td>
                                                <td>{{ $promotion->f_section->name_section }}</td>
                                                <td>{{ $promotion->t_grade->name }}</td>
                                                <td>{{ $promotion->new_academic_year }}</td>
                                                <td>{{ $promotion->t_classroom->name_class }}</td>
                                                <td>{{ $promotion->t_section->name_section }}</td>
                                                <td>

                                                    <button type="button" class="btn btn-outline-danger"
                                                        data-toggle="modal"
                                                        data-target="#Delete_one{{ $promotion->id }}">{{ trans('students_trans.back_to_student') }}
                                                    </button>
                                                    {{-- <button type="button" class="btn btn-outline-success"
                                                        data-toggle="modal" data-target="#">تخرج الطالب</button> --}}
                                                </td>

                                            </tr>
                                            @include('students.promotion.delete_all')
                                            @include('students.promotion.delete_one')
                                        @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
@endsection
@section('js')
@toastr_js
@toastr_render
@endsection
