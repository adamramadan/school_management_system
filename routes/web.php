<?php

namespace App\Http\Controllers\Grade;

namespace App\Http\Controllers;

use App\Http\Controllers\Classroom\ClassroomController;
use App\Http\Controllers\Grade\GradeController;
use App\Http\Controllers\Section\SectionController;
use App\Http\Controllers\Student\AttendanceController;
use App\Http\Controllers\Student\FeeController;
use App\Http\Controllers\Student\FeeInvoiceController;
use App\Http\Controllers\Student\GraduatedController;
use App\Http\Controllers\Student\PaymentStudentController;
use App\Http\Controllers\Student\ProcessingFeeController;
use App\Http\Controllers\Student\PromotionController;
use App\Http\Controllers\Student\ReceiptStudentController;
use App\Http\Controllers\Student\StudentController;
use Facade\FlareClient\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', function () {
        return view('auth.login');
    });
});




Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth']
    ],
    function () {

        // Route::get('/', function () {
        //     return view('dashboard');
        // });
        Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

        Route::resource('grades', GradeController::class);
        //
        Route::resource('classrooms', ClassroomController::class);
        Route::post('delete_all', [ClassroomController::class, 'delete_all'])->name('delete_all');
        Route::post('filter_classes', [ClassroomController::class, 'filter_classes'])->name('filter_classes');
        //
        Route::resource('sections', SectionController::class);
        Route::get('/classes/{id}', [SectionController::class, 'getclasses']);

        //
        Route::view('add_parent', 'livewire.show_form');

        //
        Route::resource('teachers', TeacherController::class);

        //
        Route::resource('students', StudentController::class);

        Route::get('/get_classrooms/{id}', [StudentController::class, 'get_classrooms']);
        Route::get('/get_sections/{id}', [StudentController::class, 'get_sections']);
        Route::post('upload_attachment', [StudentController::class, 'upload_attachment'])->name('upload_attachment');
        Route::get('download_attachment/{studentName}/{fileName}', [StudentController::class, 'download_attachment'])->name('download_attachment');
        Route::post('delete_attachment', [StudentController::class, 'delete_attachment'])->name('delete_attachment');

        Route::resource('promotions', PromotionController::class);
        Route::resource('graduated', GraduatedController::class);
        Route::resource('fees', FeeController::class);
        Route::resource('fees_invoices', FeeInvoiceController::class);
        Route::resource('receipt_students', ReceiptStudentController::class);
        Route::resource('processing_fees', ProcessingFeeController::class);
        Route::resource('payment_students', PaymentStudentController::class);
        Route::resource('attendance', AttendanceController::class );
        Route::resource('subjects', SubjectController::class);


    }
);

        //...
